#pragma checksum "C:\ASP-Projects\MultiplicationTable\MultiplicationTable\Views\Home\Contact.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "e455a9c27713d90ab49308854efb0319e89ccbdd"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Home_Contact), @"mvc.1.0.view", @"/Views/Home/Contact.cshtml")]
[assembly:global::Microsoft.AspNetCore.Mvc.Razor.Compilation.RazorViewAttribute(@"/Views/Home/Contact.cshtml", typeof(AspNetCore.Views_Home_Contact))]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#line 1 "C:\ASP-Projects\MultiplicationTable\MultiplicationTable\Views\_ViewImports.cshtml"
using MultiplicationTable;

#line default
#line hidden
#line 2 "C:\ASP-Projects\MultiplicationTable\MultiplicationTable\Views\_ViewImports.cshtml"
using MultiplicationTable.Models;

#line default
#line hidden
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"e455a9c27713d90ab49308854efb0319e89ccbdd", @"/Views/Home/Contact.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"7db41931e3221e991d385b1a19eb1a40727384bc", @"/Views/_ViewImports.cshtml")]
    public class Views_Home_Contact : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<dynamic>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            BeginContext(0, 2, true);
            WriteLiteral("\r\n");
            EndContext();
#line 2 "C:\ASP-Projects\MultiplicationTable\MultiplicationTable\Views\Home\Contact.cshtml"
  
    ViewData["Title"] = "Contact";

#line default
#line hidden
            BeginContext(45, 6, true);
            WriteLiteral("\r\n<h2>");
            EndContext();
            BeginContext(52, 13, false);
#line 6 "C:\ASP-Projects\MultiplicationTable\MultiplicationTable\Views\Home\Contact.cshtml"
Write(ViewBag.Title);

#line default
#line hidden
            EndContext();
            BeginContext(65, 11, true);
            WriteLiteral("</h2>\r\n<h5>");
            EndContext();
            BeginContext(77, 15, false);
#line 7 "C:\ASP-Projects\MultiplicationTable\MultiplicationTable\Views\Home\Contact.cshtml"
Write(ViewBag.Message);

#line default
#line hidden
            EndContext();
            BeginContext(92, 744, true);
            WriteLiteral(@"</h5>

<br />

<h6>Montreal:</h6>
<address>
    254 John Street<br />
    Montreal, QC H4w 2O3<br />
    <abbr title=""Phone"">P:</abbr>
    425.555.0111
</address>

<address>
    <strong>Support:</strong>   <a href=""mailto:Support@xm.com"">Support@xm.com</a><br />
    <strong>Marketing:</strong> <a href=""mailto:Marketing@xm.com"">Marketing@xm.com</a>
</address>

<br />

<h6>Ottawa:</h6>
<address>
    345 Smith Street<br />
    Ottawa, ON H3W 4F2<br />
    <abbr title=""Phone"">P:</abbr>
    525.444.1100
</address>

<address>
    <strong>Support:</strong>   <a href=""mailto:Support@xo.com"">Support@xo.com</a><br />
    <strong>Marketing:</strong> <a href=""mailto:Marketing@xo.com"">Marketing@xo.com</a>
</address>
");
            EndContext();
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<dynamic> Html { get; private set; }
    }
}
#pragma warning restore 1591
