﻿using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using MultiplicationTable.Models;

namespace MultiplicationTable.Controllers
{
    /// <summary>
    /// This controller is the main controller of the program.
    /// </summary>
    public class HomeController : Controller
    {
        /// <summary>
        /// This action is the main action of the Home controller.
        /// It shows the first page of the program web.
        /// </summary>
        /// <remarks>
        /// This action is a Get type. It is called by clicking on the Home tab.
        /// </remarks>
        public IActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// This action gives some information about the program.
        /// </summary>
        /// <remarks>
        /// This action is a Get type. It is called by clicking on the About tab.
        /// </remarks>
        public ActionResult About()
        {
            ViewData["Message"] = "X company is known the best company in Canada for year 2018.";

            return View();
        }

        /// <summary>
        /// This action gives some information about the program's owner.
        /// </summary>
        /// <remarks>
        /// This action is a Get type. It is called by clicking on the Contact tab.
        /// </remarks>
        public ActionResult Contact()
        {
            ViewData["Message"] = "We have two headquarters in Canada, Montreal and Ottawa.";

            return View();
        }
        /// <summary>
        /// This action is the privacy action of the program.
        /// It shows the page related to the right use of this web service.
        /// </summary>
        /// <remarks>
        /// This action is a HttpGet type of the Home controller.
        /// </remarks>
        public IActionResult Privacy()
        {
            return View();
        }

        /// <summary>
        /// This action is the error action of the program.
        /// If an error occurred while processing the request, 
        /// this action shows the relevant page of informaing the occurred error.
        /// </summary>
        /// <remarks>
        /// This action is a HttpGet type of the Home controller.
        /// </remarks>
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
