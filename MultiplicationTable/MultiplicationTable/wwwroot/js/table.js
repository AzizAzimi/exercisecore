﻿$(document).ready(function () {
    //Define a tooltip for every tag which uses the title attribute.
    $(document).tooltip();

    //Define some styles for the table.
    $('#multi').css("border", "2px double blue");
    $('#multi tr').css("border", "2px double blue");
    $('#multi td').css("border", "2px double blue");
    $('#multi').css("text-align", "center");

    $('#select').val(base);

    var i, j;

    //Set background color for the diagonal cells, the first row and the first column.
    var size = $('#MatrixSize').val();
    for (i = 1; i <= size + 1; i++) {
        for (j = 1; j <= size + 1; j++) {
            var str = '#multi tr:nth-child(' + i + ') td:nth-child(' + j + ')';

            if ((i == j && i > 1 && j > 1) || (i == 1 && j > 1) || (j == 1 && i > 1)) {
                $(str).css("background", "DarkTurquoise");
            }
        }
    }

    //Set the information required for the tooltip of the table's cells.
    for (i = 1; i <= size; i++) {
        for (j = 1; j <= size; j++) {
            var str = '#multi tr:nth-child(' + (i + 1) + ') td:nth-child(' + (j + 1) + ')';

            var str1 = matrix2[0][j] + 'x' + matrix2[i][0] + '=' + matrix2[i][j];
            $(str).attr("title", str1);
        }
    }

    //Find the prime numbers between 0 and (size*size).
    var primeSize = Math.pow(size, 2);
    var primes = [];
    primes.push(2);

    for (i = 3; i <= primeSize; i++) {
        var prime = true;
        for (var n = 0; n < primes.length; n++) {
            if (i % primes[n] == 0) {
                prime = false;
                break;
            }
        }
        if (prime) {
            primes.push(i);
        }
    }

    //Set background color for the table's cells involving a prime number.
    for (i = 2; i <= size + 1; i++) {
        for (j = 2; j <= size + 1; j++) {
            var num = ((i - 1) * (j - 1));
            for (var n = 1; n <= primes.length; n++) {
                if (num === primes[n]) {
                    str = '#multi tr:nth-child(' + i + ') td:nth-child(' + j + ')';
                    $(str).css("background", "LightCoral");
                    break;
                }
            }
        }
    }
});